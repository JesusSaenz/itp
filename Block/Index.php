<?php
namespace Gamma\ITP\Block;

use Magento\Framework\View\Element\Template;

class Index extends \Magento\Framework\View\Element\Template
{

    protected $_scopeConfig;

	public function __construct
    (
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    )
    {
        $this->_scopeConfig = $scopeConfig;
        parent::__construct($context);
    }

	public function getGammaTitle()
    {
        $path = 'gamma/general_configuration/title';
        $value = $this->_scopeConfig->getValue($path, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return __("<h2>" . $value . "</h2>");
    }
}